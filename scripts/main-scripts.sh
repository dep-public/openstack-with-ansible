#!/bin/bash
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
sudo systemctl restart sshd


cat > /etc/hostname <<EOF
$1
EOF


## set NTP
sudo timedatectl set-timezone America/Sao_Paulo

# user settings
sudo useradd -s /bin/bash -d /home/daniel -m daniel
sudo apt-get install sudo -y || yum install -y sudo
sudo echo "daniel ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers

## Set daniel password and allow password ssh
sudo echo -e "test123\ntest123" | passwd daniel

# Bye
sudo reboot
