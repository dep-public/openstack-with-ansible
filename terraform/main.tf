# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }
}


provider "openstack" {
  user_name   = "demo"
  tenant_name = "demo"
  password    = "b76903578aaa53327e5e91e27baafaf6aecfbeb0"
  auth_url    = "http://cvip.cloudlab.int:5000/v3"
  region      = "RegionOne"
  domain_name = "Default"
}

resource "openstack_compute_keypair_v2" "keypair" {
  name       = "keypair01"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDL1UyWThs47jIqj6yi8suDogyuRiB9t81BbdtgBefGNekoaWcQNBbKAjJjvEHRPDBJWnBEfHcJ8x61c1qTBQ3FZwMBZTNUn9cs0iVHJ28OdKEVrlYRCmtf8Dhp1EI2MdVY/IwR7rnEkCOQIGjImo8vilkSyi0L+Bqi1vQ52GGsw0xRTRJ1KMIpvkW5lm0KnHcBF0x5Yd7vIUN6VCRBERVJLHBmE23gDfEDSdp4kH1P16zALxmyquLF2RU3lNB2uJSOd6MHdIWIfV6Yjvu5mq1INXVaid4s/bt92YeglrQKS8Fp9uSsR9zUpm2XEaFTu20WmV2NnLXE4q6RpIVtFdYhaQwRoxEw/htMM5dP3R58kCIzhRDHscxzp0zSucsq17lXVZClPqNpebr+D5kUjm7ddr+ljeDNB551/70gHLqO3R2TkQtx1HKNdFt2Cx8ZvcOKfDc5hop3JxwUe+9E0hKSyVZVYtYBe8DjgisW11LM2YI7nO50XezytU5EkDvQcyU= daniel@N-20UAPF2SJ5YM"
}



resource "openstack_compute_secgroup_v2" "security_group01" {
  name        = "secgroup01"
  description = "Sample sec group"

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 80
    to_port     = 80
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 443
    to_port     = 443
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = -1
    to_port     = -1
    ip_protocol = "icmp"
    cidr        = "0.0.0.0/0"
  }

}


resource "openstack_networking_network_v2" "intnet01" {
  name           = "intnet01"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "intsubnet01" {
  name = "intsubnet01"
  network_id = openstack_networking_network_v2.intnet01.id
  cidr       = "192.168.199.0/24"
  dns_nameservers = ["10.60.0.10"]
  ip_version = 4
}


data "openstack_networking_network_v2" "clabext01" {
  name = "clabext01"
}

data "openstack_networking_subnet_v2" "ext_subnets" {
  network_id = data.openstack_networking_network_v2.clabext01.id
}

data "openstack_images_image_v2" "img" {
  name = "cirros"
}

resource "openstack_networking_router_v2" "router01" {
  name                = "router01"
  admin_state_up      = true
  external_network_id = data.openstack_networking_subnet_v2.ext_subnets.network_id
}

resource "openstack_networking_router_interface_v2" "router_interface_1" {
  router_id = openstack_networking_router_v2.router01.id
  subnet_id = openstack_networking_subnet_v2.intsubnet01.id
}

resource "openstack_networking_floatingip_v2" "floating_ip" {
  pool       = data.openstack_networking_network_v2.clabext01.name
  subnet_id = data.openstack_networking_subnet_v2.ext_subnets.id
}

resource "openstack_compute_instance_v2" "vm01" {
  name            = "vm01"
  flavor_name       = "m1.medium"
  security_groups = [ openstack_compute_secgroup_v2.security_group01.name ]
  key_pair = openstack_compute_keypair_v2.keypair.name

  block_device {
    uuid                  = data.openstack_images_image_v2.img.id
    source_type           = "image"
    volume_size           = 40
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    name = "intnet01"
  }
}


resource "openstack_compute_floatingip_associate_v2" "myip" {
  floating_ip = "${openstack_networking_floatingip_v2.floating_ip.address}"
  instance_id = "${openstack_compute_instance_v2.vm01.id}"
  fixed_ip    = "${openstack_compute_instance_v2.vm01.network.0.fixed_ip_v4}"
}

resource "openstack_dns_zone_v2" "testvm" {
  name        = "testvm.int."
  email       = "jdoe@example.com"
  description = "An example zone"
  ttl         = 3000
  type        = "PRIMARY"
}

resource "openstack_dns_recordset_v2" "vm01_testvm_int" {
  zone_id     = openstack_dns_zone_v2.testvm.id
  name        = "vm01.${openstack_dns_zone_v2.testvm.name}"
  description = "An example record set"
  ttl         = 3000
  type        = "A"
  records     = ["${openstack_networking_floatingip_v2.floating_ip.address}"]
}

output "vm01_ip"{
   value = "${openstack_networking_floatingip_v2.floating_ip.address}"
}
output "vm01_host"{
   value = "${ openstack_dns_recordset_v2.vm01_testvm_int.name }"
}
