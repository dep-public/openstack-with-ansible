# Define your config file:
Configurations are in the file "config.yml".

# Create your enviroment:

You can crete your inventory manually or using the python script, that will extract the nodes from config.yml:

```bash
python3 create_inventory.py config.yml
```

# If you are using another interface than eth0, you need to edit the templates to use another interface:

Example replacing from eth0 to eth1:
```bash
find -name *.j2 -exec sed -i 's/ansible_eth0/ansible_eth1/g' {} \;
```

# Start openstack installation:
```bash
time ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i hosts create.yml 
```

# Once openstack is installed, execute the following steps to validate the installation (connect to the controller node as root):

- Create a private key file:
```bash
ssh-keygen -q -N ""
```

- Create the keypair and security group as demo user
```bash
. demo-openrc
openstack keypair create --public-key ~/.ssh/id_rsa.pub mykey
openstack keypair list
openstack security group rule create --proto icmp default
openstack security group rule create --proto tcp --dst-port 22 default
```

- Create a provider instance:
```bash
openstack server create --flavor m1.nano --image cirros \
  --nic net-id=`openstack network show clabext01 -c id | grep " id " | awk '{print $4}'` --security-group default \
  --key-name mykey provider-instance
openstack server list  
```

- Create a self service networ, router and instance:
```bash
openstack network create selfservice
openstack subnet create --network selfservice \
  --dns-nameserver 8.8.8.8 --gateway 172.31.0.1 \
  --subnet-range 172.31.0.0/24 selfservice
  
openstack router create router
openstack router add subnet router selfservice
openstack router set router --external-gateway clabext01

openstack server create --flavor m1.nano --image cirros \
  --nic net-id=`openstack network show selfservice -c id | grep " id " | awk '{print $4}'` --security-group default \
  --key-name mykey selfservice-instance
openstack server list  
openstack console log show selfservice-instance
```

- Create a floating IP and add to self service instance:
```bash
openstack floating ip create clabext01
openstack server add floating ip selfservice-instance 10.7.0.169
openstack server list  
```

- Test cinder volumes:
```bash
openstack volume create --size 1 volume1
openstack volume create --size 1 volume2
openstack volume list
openstack server add volume provider-instance volume1
  openstack server add volume selfservice-instance volume2
openstack volume list
```

- Test heat (orchestration):
```bash

cat << EOF > heat-demo.yml
heat_template_version: 2015-10-15
description: Launch a basic instance with CirrOS image using the
             ``m1.tiny`` flavor, ``mykey`` key,  and one network.

parameters:
  NetID:
    type: string
    description: Network ID to use for the instance.

resources:
  server:
    type: OS::Nova::Server
    properties:
      image: cirros
      flavor: m1.nano
      key_name: mykey
      networks:
      - network: { get_param: NetID }

outputs:
  instance_name:
    description: Name of the instance.
    value: { get_attr: [ server, name ] }
  instance_ip:
    description: IP address of the instance.
    value: { get_attr: [ server, first_address ] }
EOF

export NET_ID=$(openstack network list | awk '/ clabext01 / { print $2 }') && echo $NET_ID
openstack stack create -t heat-demo.yml --parameter "NetID=$NET_ID" stack
openstack stack list
openstack server list
```

- Test zun (container):
```bash
. demo-openrc
openstack network list
export NET_ID=$(openstack network list | awk '/ clabext01 / { print $2 }') && echo ${NET_ID}
openstack appcontainer run --name cirros --net network=$NET_ID cirros ping 8.8.8.8
openstack appcontainer run --name centos7 --net network=$NET_ID centos:7 ping 8.8.8.8
openstack appcontainer list
openstack appcontainer exec --interactive cirros /bin/sh
openstack appcontainer exec --interactive centos7 /bin/sh
```

Clean:
```bash
openstack appcontainer stop container
openstack appcontainer delete container

openstack appcontainer stop centos7
openstack appcontainer delete centos7
```

-  Magnum
(Not working,  error as same as reported:
https://storyboard.openstack.org/#!/story/2008232)

```bash
openstack coe cluster template create kubernetes-cluster-template \
                     --image fedora-atomic-latest \
                     --external-network clabext01 \
                     --dns-nameserver 10.6.0.10 \
                     --master-flavor m1.small \
                     --flavor m1.small \
                     --labels use_podman=false \
                     --coe kubernetes

openstack coe cluster delete kubernetes-cluster
openstack coe cluster create kubernetes-cluster \
                        --cluster-template kubernetes-cluster-template \
                        --master-count 1 \
                        --node-count 1  \
                        --keypair mykey            
                        
openstack coe cluster list
openstack stack list
openstack stack failure list <stack>
ssh fedora@<vm> 'cat /var/log/heat-config/heat-config-script*'
ssh fedora@<vm> ' cat /var/log/cloud-init-output.log'

```

- Create other images and other instances (Example, debian)

```bash
wget https://cdimage.debian.org/cdimage/openstack/10.9.1-20210423/debian-10.9.1-20210423-openstack-amd64.qcow2

. admin-openrc
openstack image create \
        --container-format bare \
        --disk-format qcow2 \
        --public \
        --file debian-10.9.1-20210423-openstack-amd64.qcow2 \
        debian-10

openstack image list


. demo-openrc

cat << EOF > key.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC4tzrayU6ahMhmWuicy+oFfy//9oB+2EdbbmDfA0d+k3SpYjWVqho64/L+sQIAN0RGBJx42GkbKi8B6AriPw8omLOCk2WSYW3ymEC7n3l32M5T4cLr8LIYwoMOBZkMtRc3H62PrHgDoTJLhUOvT2ewj1SLl7iU5gQuInwPE6jWooIb8R6KMUl31qNpkafCVPz5ovw0iYbDamHQF6sq081Xl39px2345T8TofIAocyBUfCOstmAvPaD9lXIV3j9JmPhAy0oweXpxdPiQzBHXepLh/jrvHrV5ggl2iwmLgF3uzwYdFlQN6eCniBtBEcGqEacb6oP2KHfHer04WIbAMHZ eduardoefb@efb
EOF

openstack keypair create --public-key key.pub key

openstack security group rule create --proto icmp default
openstack security group rule create --proto tcp --dst-port 22 default

openstack server create --flavor m1.xlarge --image debian-10 \
  --nic net-id=`openstack network show clabext01 -c id | grep " id " | awk '{print $4}'` --security-group default \
  --key-name key debian-10      

openstack server list  

```
