#!/usr/bin/python2.7

import yaml
import sys

data = None
controller_ip = ""

with open("config.yml") as f:
	data = yaml.safe_load(f)

fp = open("hosts", "w")

role_list = ["controller", "compute", "storage"]

for role in role_list:
   fp.write("\n[" + str(role) + "]\n")
   for n in data["nodes"]:
      for r in n["roles"]:
         if r == role:   
            fp.write(n["oam_ip"] + "\n")
	

for n in data["nodes"]:
   fp.write("\n[" + str(n["name"]) + "]\n")
   fp.write(n["oam_ip"] + "\n")

fp.close()
