Workaround:

SET GLOBAL check_constraint_checks=0;
SET GLOBAL foreign_key_checks=0;
SET check_constraint_checks=0;
SET foreign_key_checks=0;

MariaDB [(none)]> SHOW VARIABLES like '%check_constraint_checks%';
+-------------------------+-------+
| Variable_name           | Value |
+-------------------------+-------+
| check_constraint_checks | OFF   |
+-------------------------+-------+
1 row in set (0.002 sec)

MariaDB [(none)]> 


SHOW VARIABLES like '%foreign_key_checks%';


show create table subnetpools


MariaDB [neutron]> ALTER TABLE subnetpools ADD COLUMN standard_attr_id BIGINT;
ERROR 1054 (42S22): Unknown column '`neutron`.`subnetpools_1`.`shared`' in 'CHECK'
MariaDB [neutron]> 


ALTER TABLE subnetpools ADD COLUMN shared BIGINT;




############################## BEGIN OF WORKROUND############################################################################################################
#1 - Transfer  password file to controler (cloud user)


#2 - Delete neutron database:
root_db=`cat passwords.yml | grep ROOT_DBPASS | awk '{print $NF}'`
mysql -u root -p${root_db} -e "DROP DATABASE neutron;"


#3 - Delete projects and users
for i in `openstack endpoint list | grep network | grep neutron | awk '{print $2}'`; do
openstack endpoint delete ${i}
done

for i in `openstack endpoint list | grep network | grep neutron | awk '{print $2}'`; do
openstack endpoint delete ${i}
done
openstack service delete neutron
openstack user delete neutron


# Execute the script in background:
root_db=`cat passwords.yml | grep ROOT_DBPASS | awk '{print $NF}'` && echo $root_db
while :; do
   for table in subnetpools subnet_dns_publish_fixed_ips; do
      mysql -u root -p${root_db} neutron -e "ALTER TABLE ${table} DROP CONSTRAINT CONSTRAINT_1;"
      mysql -u root -p${root_db} neutron -e "ALTER TABLE ${table} DROP CONSTRAINT CONSTRAINT_2;"
   done
done


# Execute playbook starting in neutron
time ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i hosts create_neutron.yml


############################## END OF WORKROUND############################################################################################################

DROP DATABASE neutron;
CREATE DATABASE neutron;
GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'localhost' IDENTIFIED BY '02ccdcdaa2c15d1bfe2dd8ace187b205ecf2c633';
GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'%' IDENTIFIED BY '02ccdcdaa2c15d1bfe2dd8ace187b205ecf2c633';


while :; do
   for table in subnetpools subnet_dns_publish_fixed_ips; do
      mysql -u root -pfc0610a0c3f877f948dae1152a1cbec991e78239 neutron -e "ALTER TABLE ${table} DROP CONSTRAINT CONSTRAINT_1;"
      mysql -u root -pfc0610a0c3f877f948dae1152a1cbec991e78239 neutron -e "ALTER TABLE ${table} DROP CONSTRAINT CONSTRAINT_2;"
   done
done



mysql -u root -pfc0610a0c3f877f948dae1152a1cbec991e78239 neutron << EOF
ALTER TABLE subnetpools DROP CONSTRAINT CONSTRAINT_1;
ALTER TABLE subnetpools DROP CONSTRAINT CONSTRAINT_2;
ALTER TABLE subnet_dns_publish_fixed_ips DROP CONSTRAINT CONSTRAINT_1;
ALTER TABLE subnet_dns_publish_fixed_ips DROP CONSTRAINT CONSTRAINT_2;
SHOW CREATE TABLE subnetpools;
SHOW CREATE TABLE subnet_dns_publish_fixed_ips;
EOF
sleep 1
done


SHOW CREATE TABLE subnet_dns_publish_fixed_ips;
